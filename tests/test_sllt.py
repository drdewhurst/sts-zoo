# copyright David Rushing Dewhurst, 2022 - present

import logging
import pathlib

import pyro
import pyro.distributions as dist

from sts_zoo.animals import SLLTFactory
from sts_zoo.canvas import plot_factory


OUTPATH = pathlib.Path("./out")
OUTPATH.mkdir(exist_ok=True, parents=True,)


def test_sllt_basic():
    size = 3
    t0 = 1
    t1 = 52

    sllt_factory = SLLTFactory(t0=t0, t1=t1, size=size,)
    sllt_model_1 = sllt_factory()
    sllt_model_2 = sllt_factory()

    logging.info(f"Created a sllt factory: {sllt_factory}")
    logging.info(f"Created a sllt model: {sllt_model_1}")
    logging.info(f"And another one: {sllt_model_2}")

    sllt1_trace = pyro.poutine.trace(sllt_model_1).get_trace()
    sllt1_trace.log_prob_sum()
    logging.info(f"Traced sllt 1:\n{sllt1_trace.nodes}")
    sllt2_trace = pyro.poutine.trace(sllt_model_2).get_trace()
    sllt2_trace.log_prob_sum()
    logging.info(f"Traced sllt 2:\n{sllt2_trace.nodes}")
    for (k1, k2) in zip(sllt1_trace.nodes.keys(), sllt2_trace.nodes.keys()):
        if  (k1 != "_INPUT") and (k1 != "_RETURN"):
            assert k1 != k2

    plot_factory(sllt_factory, OUTPATH)
    