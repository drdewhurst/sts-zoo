# copyright David Rushing Dewhurst, 2022 - present

import abc

import torch

from stsb3 import sts


class ModelFactory(abc.ABC):
    """
    Abstract base class for all model factories. Ensures unique ids for each
    component of each generated model, and encapsulates model generation through
    `make` method.
    """
    _id = 0

    def __init__(self, t0=0, t1=2, size=1,):
        self.t0 = t0
        self.t1 = t1
        self.size = size

    @abc.abstractmethod
    def __str__(self,):
        ...

    def __repr__(self,):
        return self.__str__()

    def __call__(self,):
        model = self.make()
        type(self)._id += 1
        return model

    @abc.abstractmethod
    def make(self,):
        """
        Abstract method to be overridden. Calling `.make()` should
        create a new `stsb3` model with guaranteed unique site names.
        """
        ...


class LLTFactory(ModelFactory):
    """
    A local linear trend (LLT) factory.

    The generative model for programs created by this factory is

    \\begin{aligned}
    l_t &= l_{t - 1} + \mathrm{loc}_t + \mathrm{scale\_local}_t w^l_t,\ l_0 = \mathrm{ic\_local} \\\\
    z_t &= z_{t - 1} + l_t + \mathrm{scale\_level}_t w^z_t,\ z_0 = \mathrm{ic\_level},
    \end{aligned}

    where $w^\cdot_t \sim \mathrm{Normal}(0, 1)$, $t_0 \leq t \leq t_1$.

    *Args:*

    + `loc (Block || torch.tensor || pyro.distributions)`: location parameter
    + `scale_local (Block || torch.tensor || pyro.distributions)`: scale parameter for $l_t$
    + `scale_level (Block || torch.tensor || pyro.distributions)`: scale parameter for $z_t$
    + `ic_local (torch.tensor || pyro.distributions)`: initial condition for $l_t$
    + `ic_level (torch.tensor || pyro.distributions)`: initial condition for $z_t$
    """

    def __init__(
        self,
        t0=0,
        t1=2,
        size=1,
        loc=None,
        scale_level=None,
        scale_local=None,
        ic_level=None,
        ic_local=None,
    ):
        super().__init__(t0=t0, t1=t1, size=size,)
        self.loc = loc
        self.scale_level = scale_level
        self.scale_local = scale_local
        self.ic_level = ic_level
        self.ic_local = ic_local

    def __str__(self,):
        s = f"LLTFactory(loc={self.loc}, scale_level={self.scale_level}, "
        s += f"scale_local={self.scale_local}, ic_level={self.ic_level}, "
        s += f"ic_local={self.ic_local})"
        return s

    def make(self,):
        """
        Creates a new LLT model. The sites in the created model are guaranteed
        to be unique.

        *Args:*

        None
        """
        _id = type(self)._id
        local = sts.RandomWalk(
            name=f"Local[{_id}]",
            t0=self.t0,
            t1=self.t1,
            size=self.size,
            loc=self.loc,
            scale=self.scale_local,
            ic=self.ic_local,
        )
        level = sts.RandomWalk(
            name=f"Level[{_id}]",
            t0=self.t0,
            t1=self.t1,
            size=self.size,
            loc=local,
            scale=self.scale_level,
            ic=self.ic_level,
        )
        return level


class SLLTFactory(ModelFactory):
    """
    A semi-local linear trend (SLLT) factory.

    The generative model for programs created by this factory is

    \\begin{aligned}
    l_t &= \\alpha_t + \\beta_t l_{t-1} + \mathrm{scale\_local}_t w^l_t \\\\
    z_t &= z_{t - 1} + l_t + \mathrm{scale\_level}_t w^z_t,\ z_0 = \mathrm{ic\_level},
    \end{aligned}

    where $w^\cdot_t \sim \mathrm{Normal}(0, 1)$, $t_0 \leq t \leq t_1$.

    *Args:*

    + `alpha (Block || torch.tensor || pyro.distributions)`: intercept parameter of $l_t$
    + `beta (Block || torch.tensor || pyro.distributions)`: slope parameter of $l_t$
    + `scale_local (Block || torch.tensor || pyro.distributions)`: scale parameter for $l_t$
    + `scale_level (Block || torch.tensor || pyro.distributions)`: scale parameter for $z_t$
    + `ic_level (torch.tensor || pyro.distributions)`: initial condition for $z_t$
    """

    def __init__(
        self,
        t0=0,
        t1=2,
        size=1,
        alpha=None,
        beta=None,
        scale_level=None,
        scale_local=None,
        ic_level=None,
    ):
        super().__init__(t0=t0, t1=t1, size=size,)
        self.alpha = alpha
        self.beta = beta
        self.scale_level = scale_level
        self.scale_local = scale_local
        self.ic_level = ic_level

    def __str__(self,):
        s = f"SLLTFactory(alpha={self.alpha}, beta={self.beta}, "
        s += f"scale_level={self.scale_level}, "
        s += f"scale_local={self.scale_local}, ic_level={self.ic_level})"
        return s

    def make(self,):
        """
        Creates a new SLLT model. The sites in the created model are guaranteed
        to be unique.

        *Args:*

        None
        """
        _id = type(self)._id
        local = sts.AR1(
            name=f"SemiLocal[{_id}]",
            t0=self.t0,
            t1=self.t1,
            size=self.size,
            alpha=self.alpha,
            beta=self.beta,
            scale=self.scale_local,
        )
        level = sts.RandomWalk(
            name=f"Level[{_id}]",
            t0=self.t0,
            t1=self.t1,
            size=self.size,
            loc=local,
            scale=self.scale_level,
            ic=self.ic_level,
        )
        return level


class SGTFactory(ModelFactory):
    """
    A seasonal + global trend (SGT) factory.

    The generative model for programs created by this factory is

    \\begin{aligned}
    l_t &= \\alpha_t + \\beta_t l_{t-1} + \mathrm{scale\_local}_t w_t \\\\
    g_t &= a + bt \\\\
    s_t &= \\theta_{t \mathrm{mod} s},\ s=1,...,\mathrm{num\_seasons}\\\\
    z_t &= s_t + g_t + l_t,
    \end{aligned}

    where $w_t \sim \mathrm{Normal}(0, 1)$, $t_0 \leq t \leq t_1$,
    and $\mathrm{num\_seasons}$ is the number of discrete seasons.

    *Args:*

    + `alpha (Block || torch.tensor || pyro.distributions)`: intercept parameter of $l_t$
    + `beta (Block || torch.tensor || pyro.distributions)`: slope parameter of $l_t$
    + `a (Block || torch.tensor || pyro.distributions)` intercept parameter of $g_t$
    + `b (Block || torch.tensor || pyro.distributions)` slope parameter of $g_t$
    + `num_seasons (int)`: $\geq 2$ integer number of seasons, defaults to 2
    + `seasons (torch.tensor || pyro.distributions)` season values
    + `scale_local (Block || torch.tensor || pyro.distributions)`: scale parameter for $l_t$
    + `scale_level (Block || torch.tensor || pyro.distributions)`: scale parameter for $z_t$
    + `ic_level (torch.tensor || pyro.distributions)`: initial condition for $z_t$
    """

    def __init__(
        self,
        t0=0,
        t1=2,
        size=1,
        alpha=None,
        beta=None,
        a=None,
        b=None,
        num_seasons=2,
        seasons=None,
        scale_level=None,
        scale_local=None,
        ic_level=None,
    ):
        super().__init__(t0=t0, t1=t1, size=size,)
        self.alpha = alpha
        self.beta = beta
        self.a = a
        self.b = b
        self.num_seasons = num_seasons
        self.seasons = seasons
        self.scale_level = scale_level
        self.scale_local = scale_local
        self.ic_level = ic_level

    def __str__(self,):
        s = f"SGTFactory(alpha={self.alpha}, beta={self.beta}, "
        s += f"a={self.a}, b={self.b}, num_seasons={self.num_seasons}, "
        s += f"seasons={self.seasons}, "
        s += f"scale_level={self.scale_level}, "
        s += f"scale_local={self.scale_local}, ic_level={self.ic_level})"
        return s

    def make(self,):
        """
        Creates a new SGT model. The sites in the created model are guaranteed
        to be unique.

        *Args:*

        None
        """
        _id = type(self)._id
        local_noise = sts.AR1(
            name=f"SemiLocal[{_id}]",
            t0=self.t0,
            t1=self.t1,
            size=self.size,
            alpha=self.alpha,
            beta=self.beta,
            scale=self.scale_local,
        )
        gt = sts.GlobalTrend(
            name=f"GlobalTrend[{_id}]",
            t0=self.t0,
            t1=self.t1,
            size=self.size,
            alpha=self.a,
            beta=self.b,
        )
        seasons = sts.DiscreteSeasonal(
            name=f"DiscreteSeasonal[{_id}]",
            t0=self.t0,
            t1=self.t1,
            size=self.size,
            n_seasons=self.num_seasons,
            seasons=self.seasons,
        )
        return seasons + gt + local_noise


class StochVolFactory(ModelFactory):
    """
    A stochastic volatility model factory.

    This is a model for the log of an asset price $S_t$. Define $z_t = \log S_t$.
    The generative model for programs created by this factory is

    \\begin{aligned}
    \mathrm{isp}\ \sigma_t &= \mathrm{isp}\ \sigma_{t - 1} + \mathrm{d}t_t\ \mathrm{loc\_ispvol}_t\ +
         \sqrt{\mathrm{d}t_t}\ \mathrm{scale\_ispvol}_t\ w^{\mathrm{isp}\sigma}_t,\ l_0 = \mathrm{ic\_logvol} \\\\
    z_t &= z_{t - 1} + \mathrm{d}t_t\mathrm{loc}_t + \sqrt{\mathrm{d}t_t}\sigma_t w^z_t,\ z_0 = \mathrm{ic},
    \end{aligned}

    where $w^\cdot_t \sim \mathrm{Normal}(0, 1)$, $t_0 \leq t \leq t_1$, and $\mathrm{isp}$ is the inverse softplus function,
    $\mathrm{isp}(y) = \log(-1 + \exp y)$. 

    *Args:*

    + `dt (Block || torch.tensor || pyro.distributions)`: time discretization. If a `pyro.distributions` object, it should
        be an atomless distribution with non-negative support and will be interpreted as
        $p(dt) = \prod_{t_0\leq t\leq t_1} p(dt_t)$, not as a single draw. Passing a distributions object can be a useful method for modeling
        jump-y asset prices (e.g., $p(dt_t) = \mathrm{Gamma}(dt_t | \\alpha, \\beta)$ generates the variance gamma process).
        However, doing this also makes the model non-identifiable in the scale parameter(s).
    + `loc_ispvol (Block || torch.tensor || pyro.distributions)`: location parameter of the isp volatility process
    + `vol_ispvol (Block || torch.tensor || pyro.distributions)`: scale/volatility parameter of the isp volatility process
    + `ic_ispvol (torch.tensor || pyro.distributions)`: initial condition of the isp_volatility process
    + `scale_level (Block || torch.tensor || pyro.distributions)`: scale parameter for $z_t$
    + `loc (Block || torch.tensor || pyro.distributions)`: location parameter for the log asset price process.
    + `ic (torch.tensor || pyro.distributions)`: initial condition for the log asset price process
    """

    def __init__(
        self,
        t0=0,
        t1=2,
        size=1,
        dt=torch.tensor(0.01),
        loc_ispvol=None,
        vol_ispvol=None,
        ic_ispvol=None,
        loc=None,
        ic=None,
    ):
        super().__init__(t0=t0, t1=t1, size=size,)
        self.dt = dt
        self.loc_ispvol = loc_ispvol
        self.vol_ispvol = vol_ispvol
        self.ic_ispvol = ic_ispvol
        self.loc = loc
        self.ic = ic

    def __str__(self,):
        s = f"StochVolFactory(dt={self.dt}, loc_logvol={self.loc_ispvol}, "
        s += f"vol_logvol={self.vol_ispvol}, "
        s += f"ic_logvol={self.ic_ispvol}, loc={self.loc}, "
        s += f"ic={self.ic})"
        return s

    def make(self,):
        """
        Creates a new StochVol model. The sites in the created model are guaranteed
        to be unique.

        *Args:*

        None
        """
        _id = type(self)._id
        ispvol = sts.CCSDE(
            name=f"LogVol[{_id}]",
            t0=self.t0,
            t1=self.t1,
            size=self.size,
            loc=self.loc_ispvol,
            scale=self.vol_ispvol,
            dt=self.dt,
            ic=self.ic_ispvol,
        )
        log_return = sts.CCSDE(
            name=f"LogReturn[{_id}]",
            t0=self.t0,
            t1=self.t1,
            size=self.size,
            loc=self.loc,
            scale=ispvol.softplus(),
            ic=self.ic,
        )
        return log_return
