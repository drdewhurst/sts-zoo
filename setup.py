from setuptools import setup

setup(
    name="sts_zoo",
    version="0.1.0",
    description="Exotic structural time series animals",
    url="https://gitlab.com/drdewhurst/sts-zoo",
    author="David Dewhurst",
    author_email="drd@davidrushingdewhurst.com",
    license="MIT",
    packages=["sts_zoo"],
    install_requires=[
        "black",
        "numpy",
        "opt-einsum",
        "pyflakes",
        "pyro-api",
        "pyro-ppl",
        "pytest",
        "stsb3",
        "torch",
        "virtualenv",
    ],
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Science/Research",
        "Intended Audience :: Financial and Insurance Industry",
        "License :: OSI Approved :: MIT License",
        "Operating System :: POSIX",
        "Topic :: Scientific/Engineering :: Artificial Intelligence",
    ],
)
